Midterm Exam - Gitea in Azure with DevOps Tools

Objective

The goal is to set up Gitea in Azure using Docker, Terraform, and Ansible, ensuring the setup works after a VM reboot, utilizes DNS and SSL, and is documented thoroughly.

Requirements

	•	Use Docker to run Gitea.
	•	Ensure the setup functions properly after a VM reboot.
	•	Use DNS and proper SSL setup (mohamed.chas.dsnw.dev).
	•	Document the process as a how-to guide.
	•	Create the Gitea container using GitLab CI/CD and store it in the GitLab Container registry.
	•	Deploy infrastructure with Terraform, automate the deployment process with Ansible.
	•	Compile Gitea code from scratch, produce binaries, and turn them into a Docker image.


Prerequisites

	•	Azure account
	•	GitLab account
	•	Docker installed
	•	Terraform installed
	•	Ansible installed

Step-by-Step Guide

Step 1: Setup Azure VM

	1.	Create an Azure VM with the following details:
	•	Name: gitea-vm
	•	Resource Group: gitea-rg
	•	Public IP Address: gitea-pip
	•	Network Interface: gitea-nic
	•	Operating System: Linux

Step 2: Clone the Repository

git clone https://gitlab.com/Morghani/mid_term.git
cd mid_term

Step 3: Compile Gitea from Source and Create Docker Image

	1.	Install necessary dependencies:

./Scripts/install_dependencies.sh

	2.	Navigate to the Docker directory and build the Docker image:

cd Docker
docker build -t registry.gitlab.com/Morghani/midterm/gitea:latest .

	3.	Push the Docker image to GitLab Container Registry:

docker push registry.gitlab.com/Morghani/midterm/gitea:latest

Step 4: Configure GitLab CI/CD Pipeline

	1.	Ensure the .gitlab-ci.yml file is configured to build and push the Docker image. Here is a sample configuration:

image: docker:latest

services:
  - docker:dind

stages:
  - build
  - push

variables:
  DOCKER_DRIVER: overlay2

before_script:
  - docker info

build:
  stage: build
  script:
    - docker build -t registry.gitlab.com/Morghani/midterm/gitea:latest .

push:
  stage: push
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - docker push registry.gitlab.com/Morghani/midterm/gitea:latest

Step 5: Setup Infrastructure with Terraform

	1.	Navigate to the Terraform directory and initialize Terraform:

cd Terraform
terraform init

	2.	Apply the Terraform configuration:

terraform apply

This will create the necessary infrastructure on Azure.

Step 6: Automate Deployment with Ansible

	1.	Update the hosts.ini file with your Azure VM details.
	2.	Run the Ansible playbook to deploy Gitea, Nginx, Prometheus, and Grafana:

cd Ansible
ansible-playbook -i hosts.ini playbook.yml

Step 7: Configure Nginx for SSL

	1.	Ensure the setup_ssl.sh script is configured correctly and run it:

./Scripts/setup_ssl.sh

Step 8: Verify the Setup

	1.	Access Gitea via the domain mohamed.chas.dsnw.dev.
	2.	Verify Prometheus and Grafana are running and accessible.

Conclusion

Following these steps, you should have a fully functional Gitea setup on Azure, using Docker, Terraform, and Ansible, with proper DNS and SSL configuration. This setup ensures the system remains functional after VM reboots and includes monitoring with Prometheus and Grafana.